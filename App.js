import React, {Component} from 'react';
import {StatusBar, YellowBox} from 'react-native';
import {Provider} from 'react-redux';
import createStore from './src/store/createStore';
import {PersistGate} from 'redux-persist/lib/integration/react';
import AppWithNavigationState from './src/navigators/AppNavigator';
import NavigationService from './src/navigators/NavigationService';
import LaunchScreen from './src/screens/LaunchScreen/index_without_connect';

import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';


const {store, persistor} = createStore();
console.log("store",store);
console.log("persistor",persistor)

YellowBox.ignoreWarnings([
  'Warning: componentWillReceiveProps has been renamed',
  'Warning: AsyncStorage has been extracted from react-native core and will be removed in a future release',
]);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    };
  }


  render() {
    return (
      <Provider store={store}>
        <NavigationContainer ref={NavigationService._navigator}>
          <PersistGate loading={<LaunchScreen />} persistor={persistor}>
              <StatusBar backgroundColor="white" barStyle="dark-content"/>
              <AppWithNavigationState/>
          </PersistGate>
        </NavigationContainer>
      </Provider>
    );
  }
}

export default App;
