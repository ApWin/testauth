import React, { Component } from 'react'
import {View, StatusBar, Text} from 'react-native';
import Components from 'components'
import styles from './styles'
import {Routines} from 'services/api';
import get from "lodash/get";
import NavigationService from "../../navigators/NavigationService";
import {LOGOUT} from "../../store/constants";
import {connect} from 'react-redux';
import LoginScreen from "../LoginScreen";

class ScreenStructure extends Component{
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.getProjects()
    }
    getProjects(){
        Routines.user.getProjects({
            request: {}
        }, this.props.dispatch)
            .then((e)=>{
                console.log('Success responsee:', e)
            }).catch((error)=>{
                console.log('Error in response:', error)
        })
    }

    render () {
        let { storedProjects } = this.props
        console.log('STORED DATA:', storedProjects)
        return (
            <Components.Layout>
             <Text>
                 This is screenStructure !!!
             </Text>
            </Components.Layout>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        storedProjects: state.profile.projects
    };
};

ScreenStructure = connect(mapStateToProps)(ScreenStructure);

export default ScreenStructure;
