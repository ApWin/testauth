import { StyleSheet } from 'react-native';
import colors from "assets/styles/colors";
import {container, center} from '../../assets/styles/common';

export default StyleSheet.create({
    container:{
        ...container,
        ...center,
        backgroundColor: '#F4F4F4',
    },
    title:{
        textTransform:'uppercase',
        fontWeight:'700',
        fontSize:28,
        marginBottom:0,
        color:'#000'
    },
    text:{
        fontSize:16,
        color:'#000'
    },
    form:{
        paddingHorizontal:15,
        paddingVertical:20
    },
    input:{
        borderWidth:1,
        borderColor:'#95989A',
        borderStyle:'solid',
        // textTransform:'uppercase',
        color:'#000',
        backgroundColor:'#fff',
        padding:10,
        borderRadius:8,
        fontSize:20,
        width:300,
        marginTop:15,
        marginHorizontal:'auto'
    },
    btn:{
        marginTop:20,
        borderRadius:8,
        padding:10,
        textAlign:'center',
        backgroundColor:'#000',
        width:300,
    },
    btnText:{
        color:'#fff',
        fontSize:24,
        textAlign:'center',
        textTransform:'uppercase',
    }
})
