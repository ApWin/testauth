import React,{useState} from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity
} from "react-native";
import styles from "./style";
import get from "lodash/get";
import {Routines} from "../../services/api";
import {connect} from 'react-redux';
import NavigationService from "navigators/NavigationService";

class LoginScreen extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            username:'',
            password:''
        }
    }
    onSubmit(){
        Routines.user.login({request:{
                data: {
                    username:this.state.username,
                    password:this.state.password
                }
            }}, this.props.dispatch)
            .then((e) => {
                let status = get(e, `response.status`, 200)
                let res = get(e, `response.data`, [])
                console.log("message for test", res);
                if (status===200){
                    NavigationService.navigate('screenStructure')
                }


            })
            .catch(e => {
                console.log("message for Error catching", e);

            });
    }

    render() {
        let { username, password } = this.state
        return (
            <View style={
                styles.container
            }>
                <Text style={
                    styles.title
                }>
                    авторизация
                </Text>
                <Text style={
                    styles.text
                }>
                    Введите свой логин и пароль
                </Text>
                <View style={
                    styles.form
                }>
                    <TextInput
                        style={styles.input}
                               value={username}
                               onChangeText={
                                   (text) => {
                                       this.setState({
                                           username:text
                                       })
                                   }
                               }
                               placeholder="ИМЯ ПОЛЬЗОВАТЕЛЯ"/>
                    <TextInput style={
                        styles.input
                    }
                               value={password}
                               secureTextEntry={true}
                               onChangeText={
                                   (text) => {
                                       this.setState({
                                           password:text
                                       })
                                   }
                               }
                               placeholder="ПАРОЛЬ"/>
                    <TouchableOpacity
                        style={styles.btn}
                        onPress={()=>{
                            this.onSubmit()
                            }}
                    >
                        <Text style={styles.btnText}>Войти</Text>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {

    };
};

LoginScreen = connect(mapStateToProps)(LoginScreen);
export default LoginScreen;
