import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import commonOptions from './CommonOptions';

/* ------------- SCREENS ------------- */
import ScreenStructure from '../screens/ScreenStructure';
import LaunchScreen from "../screens/LaunchScreen";

import SignIn from "../screens/LoginScreen/index";
/* ------------- SCREENS ------------- */

const Stack = createStackNavigator();

function AppNavigator() {
    return (
        <Stack.Navigator>
            <Stack.Screen options={{...commonOptions}} name='launchScreen' component={LaunchScreen}/>
            <Stack.Screen options={{...commonOptions}} name='loginScreen' component={SignIn}/>
            <Stack.Screen options={{...commonOptions}} name='screenStructure' component={ScreenStructure}/>
        </Stack.Navigator>
    );
}

export default AppNavigator;
