import * as React from "react";
import Svg, { Circle, Path } from "react-native-svg";
import {perfectSize} from '../../styles/helper';

function SvgComponent(props) {
    return (
        <Svg width={perfectSize(265)} height={perfectSize(265)} viewBox="0 0 265 265" fill="none" {...props}>
            <Circle cx={132.5} cy={132.5} r={132.5} fill="#E1084D" />
            <Path
                d="M187.422 127.553L106.533 81.653C104.966 80.7656 103.055 80.786 101.508 81.6938C99.9504 82.6118 99 84.2846 99 86.1002V177.9C99 179.716 99.9504 181.388 101.508 182.306C102.296 182.765 103.176 183 104.056 183C104.905 183 105.764 182.786 106.533 182.347L187.422 136.447C189.009 135.539 190 133.846 190 132C190 130.154 189.009 128.461 187.422 127.553Z"
                fill="white"
            />
        </Svg>
    );
}

export default SvgComponent;
