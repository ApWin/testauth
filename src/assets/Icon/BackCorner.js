import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={368} height={247} viewBox="0 0 368 247" fill="none" {...props}>
            <Path
                d="M-169.603 -376.119C-144.395 -430.177 -80.1371 -453.565 -26.0788 -428.357L269.337 -290.603C323.395 -265.395 346.783 -201.137 321.575 -147.079L183.821 148.337C158.613 202.395 94.3549 225.783 40.2966 200.575L-255.119 62.8206C-309.177 37.6128 -332.565 -26.6451 -307.357 -80.7034L-169.603 -376.119Z"
                fill="#DB074A"
            />
        </Svg>
    );
}

export default SvgComponent;
