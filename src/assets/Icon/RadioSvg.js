import * as React from "react";
import Svg, { Circle } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={18} height={18} viewBox="0 0 18 18" fill="none" {...props}>
            {
                props.isActive ? (
                    <Circle cx={9} cy={9} r={5} fill="white" />
                ) : null
            }
            <Circle cx={9} cy={9} r={8.5} stroke="white" />
        </Svg>
    );
}

SvgComponent.defaultProps = {
    isActive: false
}

export default SvgComponent;

