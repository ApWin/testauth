import React from 'react'
import Svg, { Rect, Path } from "react-native-svg"

export default function() {
    return (
        <Svg width="15.217" height="15.218">
            <Path
                data-name="Path 1036"
                d="M9.499 1.627a5.564 5.564 0 1 0-.761 8.5c-.823-.748.15.422.318.59l3.159 3.159a1.175 1.175 0 0 0 1.662-1.662l-3.159-3.16c-.167-.166-.921-.985-.589-.317a5.569 5.569 0 0 0-.63-7.11zm-1 6.873a4.153 4.153 0 1 1 0-5.873 4.158 4.158 0 0 1 0 5.873z"
                fill="#7D7B9E"
                opacity="0.4"
            />
        </Svg>
    )
}
