import * as React from "react";
import Svg, { Path, Mask, G, Rect } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={24} height={24} viewBox="0 0 24 24" fill="none" {...props}>
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M5 21H10C10.5523 21 11 21.4477 11 22C11 22.5523 10.5523 23 10 23H5C3.34315 23 2 21.6569 2 20V4C2 2.34315 3.34315 1 5 1H10C10.5523 1 11 1.44772 11 2C11 2.55228 10.5523 3 10 3H5C4.44772 3 4 3.44772 4 4V20C4 20.5523 4.44772 21 5 21ZM9 11H21C21.5523 11 22 11.4477 22 12C22 12.5523 21.5523 13 21 13H9C8.44772 13 8 12.5523 8 12C8 11.4477 8.44772 11 9 11Z"
                fill="black"
            />
            <Mask
                id="mask0"
                mask-type="alpha"
                maskUnits="userSpaceOnUse"
                x={2}
                y={1}
                width={19}
                height={22}
            >
                <Path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M5 21H10C10.5523 21 11 21.4477 11 22C11 22.5523 10.5523 23 10 23H5C3.34315 23 2 21.6569 2 20V4C2 2.34315 3.34315 1 5 1H10C10.5523 1 11 1.44772 11 2C11 2.55228 10.5523 3 10 3H5C4.44772 3 4 3.44772 4 4V20C4 20.5523 4.44772 21 5 21ZM8.92857 11H20.0714C20.5843 11 21 11.4477 21 12C21 12.5523 20.5843 13 20.0714 13H8.92857C8.41574 13 8 12.5523 8 12C8 11.4477 8.41574 11 8.92857 11Z"
                    fill="white"
                />
            </Mask>
            <G mask="url(#mask0)">
                <Rect width={24} height={24} fill="white" fillOpacity={0.8} />
            </G>
            <Path
                d="M17.2929 15.2929C16.9024 15.6834 16.9024 16.3166 17.2929 16.7071C17.6834 17.0976 18.3166 17.0976 18.7071 16.7071L22.7071 12.7071C23.0976 12.3166 23.0976 11.6834 22.7071 11.2929L18.7071 7.29289C18.3166 6.90237 17.6834 6.90237 17.2929 7.29289C16.9024 7.68342 16.9024 8.31658 17.2929 8.70711L20.5858 12L17.2929 15.2929Z"
                fill="#CCCCCC"
            />
        </Svg>
    );
}

export default SvgComponent;
