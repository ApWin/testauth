import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={17} height={17} viewBox="0 0 17 17" fill="none" {...props}>
            <Path
                d="M8.50002 3.46651e-05C13.1944 3.46651e-05 17 3.80562 17 8.50002C17 13.1944 13.1944 17 8.50002 17C3.80562 17 3.52569e-05 13.1944 3.52569e-05 8.50002C-0.0133224 3.81893 3.77058 0.0133923 8.45162 3.46651e-05C8.46776 -1.1555e-05 8.48389 -1.1555e-05 8.50002 3.46651e-05Z"
                fill="#3BB54A"
            />
            <Path
                d="M13.1143 6.19047L7.19857 12.1429L3.88574 8.83334L5.23455 7.50001L7.19857 9.4524L11.7655 4.85714L13.1143 6.19047Z"
                fill="white"
            />
        </Svg>
    );
}

export default SvgComponent;
