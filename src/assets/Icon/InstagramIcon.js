import React from "react";
import Svg, { G, Rect, Path, Circle } from "react-native-svg";
import colors from '../../styles/colors';
/* SVGR has dropped some elements not supported by react-native-svg: title */

const SvgComponent = props => (
    <Svg width="40px" height="38px" viewBox="0 0 45 38" {...props}>
        <G id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
            <G id="biz12" transform="translate(-215.000000, -187.000000)">
                <G id="Group-5" transform="translate(60.000000, 182.000000)">
                    <G id="Group-8" transform="translate(155.000000, 5.000000)">
                        <Rect
                            id="Rectangle-Copy-86"
                            fill="#2D2A5C"
                            x={0}
                            y={0}
                            width={45}
                            height={38}
                            rx={19}
                        />
                        <G
                            id="instagram"
                            transform="translate(14.000000, 10.000000)"
                            fill={colors.brandColor}
                            fillRule="nonzero"
                        >
                            <G id="Group_8">
                                <G id="Group_7">
                                    <Path
                                        d="M11.8388571,0 L5.38114286,0 C2.40941577,0.000473242638 0.000473242638,2.40941577 0,5.38114286 L0,11.8388571 C0.000473242638,14.8105842 2.40941577,17.2195268 5.38114286,17.22 L11.8388571,17.22 C14.8105842,17.2195268 17.2195268,14.8105842 17.22,11.8388571 L17.22,5.38114286 C17.2195268,2.40941577 14.8105842,0.000473242638 11.8388571,0 Z M15.606,11.8388571 C15.6036382,13.9184136 13.9184136,15.6036382 11.8388571,15.606 L5.38114286,15.606 C3.30158638,15.6036382 1.61636179,13.9184136 1.614,11.8388571 L1.614,5.38114286 C1.61636179,3.30158638 3.30158638,1.61636179 5.38114286,1.614 L11.8388571,1.614 C13.9184136,1.61636179 15.6036382,3.30158638 15.606,5.38114286 L15.606,11.8388571 Z"
                                        id="Path_6"
                                    />
                                </G>
                            </G>
                            <G id="Group_10" transform="translate(4.000000, 4.000000)">
                                <G id="Group_9">
                                    <Path
                                        d="M4.60890909,0.5 C2.33962126,0.5 0.5,2.33962126 0.5,4.60890909 C0.5,6.87819692 2.33962126,8.71781818 4.60890909,8.71781818 C6.87819692,8.71781818 8.71781818,6.87819692 8.71781818,4.60890909 C8.71736648,2.33980839 6.87800979,0.500451697 4.60890909,0.5 Z M4.60890909,7.17718182 C3.19049123,7.17718182 2.04063636,6.02732695 2.04063636,4.60890909 C2.04063636,3.19049123 3.19049123,2.04063636 4.60890909,2.04063636 C6.02732695,2.04063636 7.17718182,3.19049123 7.17718182,4.60890909 C7.17537874,6.02657942 6.02657942,7.17537874 4.60890909,7.17718182 Z"
                                        id="Path_7"
                                    />
                                </G>
                            </G>
                            <G id="Group_12" transform="translate(13.000000, 3.000000)">
                                <G id="Group_11">
                                    <Circle id="Ellipse_6" cx={0.669} cy={0.669} r={1} />
                                </G>
                            </G>
                        </G>
                    </G>
                </G>
            </G>
        </G>
    </Svg>
);

export default SvgComponent;
