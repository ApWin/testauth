import * as React from "react";
import Svg, { G, Circle, Rect } from "react-native-svg";
import {perfectSize} from '../../styles/helper';
/* SVGR has dropped some elements not supported by react-native-svg: title */

function SvgComponent(props) {
    return (
        <Svg width={perfectSize(265)} height={perfectSize(265)} viewBox="0 0 265 265" {...props}>
            <G
                id="Page-1"
                stroke="none"
                strokeWidth={1}
                fill="none"
                fillRule="evenodd"
            >
                <G id="biz-09" transform="translate(-1292.000000, -406.000000)">
                    <G id="Group-4" transform="translate(948.000000, 406.000000)">
                        <G id="Group-3" transform="translate(344.000000, 0.000000)">
                            <Circle
                                id="Oval-Copy"
                                fill="#E1084D"
                                cx={132.5}
                                cy={132.5}
                                r={132.5}
                            />
                            <G
                                id="pause"
                                transform="translate(90.000000, 82.000000)"
                                fill="#FFFFFF"
                                fillRule="nonzero"
                            >
                                <Rect id="Rectangle" x={58} y={0} width={28} height={101} />
                                <Rect id="Rectangle" x={0} y={0} width={28} height={101} />
                            </G>
                        </G>
                    </G>
                </G>
            </G>
        </Svg>
    );
}

export default SvgComponent;
