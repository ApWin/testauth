import {
    getMe,
    getProjects
} from 'services/api/User/routines';
import get from 'lodash/get'
import { LOGOUT, SET_URL } from "../constants";
import TokenStorage from 'services/TokenStorage'
import config from "../../services/config";
import {Api} from "../../services/api";

const initial = {
    unSendActions: [],
    data: {

    },
    projects:{},
    baseUrl: config.API_ROOT
};

export default (state = initial, action) => {

    switch (action.type){

        case getMe.SUCCESS:{
            console.log("GET ME")
            let data = get(action, 'payload.response.data', {});

            // console.log("getMe", data)

            return {
                ...state,
                data
            }
        }
        case getProjects.SUCCESS:{
            let data = get(action, 'payload.response.data.results', {});
            return {
                ...state,
                projects: data
            }
        }
        case SET_URL: {
            console.log("SET_URL")
            Api.setBaseURL(action.payload)
            return {
                ...state,
                baseUrl: action.payload
            }
        }
        case LOGOUT: {
            TokenStorage.clear();
            console.log("LOGOUT")
            return {
                ...initial,
                baseUrl: state.baseUrl
            }
        }
    }
    return state
}
