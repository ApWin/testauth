import buildUrl from 'build-url'

export default (api) => {
    return {
        getMe: () => {
            let url = buildUrl(`/accounts/me/`, {
                queryParams: {

                }
            });
            return api.get(url)
        },
        login: (data) => {
            return api.post(`/token/`, data.data)
        },
        getProjects:()=>{
            let url = buildUrl(`/projects/?limit=10&offset=10`, {
                queryParams: {

                }
            });
            return api.get(url)
        }
    }
}

