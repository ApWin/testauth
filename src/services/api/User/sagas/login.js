import { call, all, put, takeEvery } from 'redux-saga/effects'

import { login } from '../routines'
import TokenStorage from '../../../TokenStorage';
import get from 'lodash/get'

function * trigger (api, action) {
    const { request } = action.payload;
    // console.log(JSON.stringify(request));
    try {
        yield put(login.request());

        const response = yield call(api.user.login, request);
        console.log("response in Saga", response.data.access)
        yield all([
            call(TokenStorage.set, get(response, `data.access`, "")),
            put(
                login.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        console.log(e);
        yield put(login.failure(e))
    } finally {
        yield put(login.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(login.TRIGGER, trigger, api)
}
