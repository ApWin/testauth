import { all } from "redux-saga/effects";

import getMe from "./getMe";
import login from './login';
import getProjects from "./getProjects";

export default function * sagas (api) {
    yield all(
        [
            getMe(api),
            login(api),
            getProjects(api)
        ]
    )
}
