import { call, all, put, takeEvery } from 'redux-saga/effects'

import { getProjects } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    console.log(JSON.stringify(request));
    try {
        yield put(getProjects.request());

        const currentToken = yield call(TokenStorage.get);

        if(currentToken)
            yield call(api.setToken, currentToken);

        const response = yield call(api.user.getProjects, request);
        console.log(JSON.stringify(response.data));
        yield all([
            put(
                getProjects.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        console.log(e);
        yield put(getProjects.failure(e))
    } finally {
        yield put(getProjects.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(getProjects.TRIGGER, trigger, api)
}
