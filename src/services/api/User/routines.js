import { createRoutine, promisifyRoutine } from 'redux-saga-routines'

export const getMe = createRoutine(('GET_ME'))
export const login = createRoutine('USER_LOGIN')
export const getProjects = createRoutine('GET_PROJECTS');

export default {
    getMe: promisifyRoutine(getMe),
    login: promisifyRoutine(login),
    getProjects: promisifyRoutine(getProjects)
}
