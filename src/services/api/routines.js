import user from './User/routines'
import filemanager from './Filemanager/routines'
import login from './Login/routines'

export default {
    user,
    filemanager,
    login
}
