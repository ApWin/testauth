import buildUrl from 'build-url'

export default (api) => {
    return {
        login: () => {
            let url = buildUrl(`/token/`, {
                queryParams: {

                }
            });
            return api.login(url)
        }
    }
}

