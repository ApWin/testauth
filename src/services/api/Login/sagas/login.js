import { call, all, put, takeEvery } from 'redux-saga/effects'

import { getMe } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    console.log(JSON.stringify(request));
    try {
        yield put(login.request());

        const currentToken = yield call(TokenStorage.get);

        if(currentToken)
            yield call(api.setToken, currentToken);

        const response = yield call(api.login.login, request);
        console.log(JSON.stringify(response.data));
        yield all([
            put(
                login.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        console.log(e);
        yield put(login.failure(e))
    } finally {
        yield put(login.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(login.TRIGGER, trigger, api)
}
