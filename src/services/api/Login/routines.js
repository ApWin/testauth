import { createRoutine, promisifyRoutine } from 'redux-saga-routines'

export const login = createRoutine(('LOGIN'))

export default {
    login: promisifyRoutine(login)
}
